# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you
num = [1, 3, 4, 5]
def calculate_average(values):
    if len(values) == 0:
        return None
    else:
        value_len = len(values)
        value_len = int(value_len)
        return sum(values) / value_len


print(calculate_average(num))
