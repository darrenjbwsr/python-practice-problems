# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

attendees_list = ["a", "s", "d", "e"]
members_list = ["b", "c"]

def has_quorum(attendees_list, members_list):
    attendees_lists = len(attendees_list)
    members_lists = len(members_list)

    if attendees_lists >= members_lists * 0.5:
        return True


print(has_quorum(attendees_list,members_list))
