# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

n = (1,2,3,33)

def find_second_largest(values):
    if values == 0:
        return None
    mx = (0)
    second_max = (0)
    for num in values:
        if num > mx:
            second_max = mx
            mx = num
        elif num > second_max and mx != num:
            second_max = num
    return second_max
print(find_second_largest(n))
